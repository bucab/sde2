conda install -c conda-forge -c bioconda STAR=2.7.1a samtools trimmomatic fastqc cython ucsc-bigwigaverageoverbed jellyfish bedtools blat bedops trinity cd-hit

pip install -r requirements.txt
