# get other regions of interest from annotation
from collections import namedtuple, defaultdict, Counter
from intervaltree import IntervalTree
from csv import reader
import json
import pandas
from pathlib import Path
import pysam

GTFRec = namedtuple('GTFRec',('chrm','src','feature','start','end','phase','strand','score','attr'))

def cast_int(x) :
    try:
        return int(x)
    except :
        return x

def gtf_transcript_to_pseudopsl(qName, recs) :
    trx_rec = [_ for _ in recs if _.feature == 'transcript']
    #assert len(trx_rec) == 1
    trx_rec = trx_rec[0]
    
    # this is a pseudo PSL; does not have complete alignment info
    trx_features = [_ for _ in recs if _.feature != 'transcript']
    trx_features = sorted(trx_features,key=lambda x: x.start)
    
    # use an interval tree to compute transcribed feature bounds
    trx_tree = IntervalTree()
    for trx in trx_features :
        trx_tree[trx.start:trx.end] = trx
    
    trx_tree.merge_overlaps()
    

    trx_features = list(sorted(trx_tree))
    
    trx_start = trx_rec.start
    trx_end = trx_rec.end
    trx_len = trx_end - trx_start
    
    blocks = defaultdict(list)

    for it in sorted(trx_features) :
        blocks['tStarts'].append(it.begin)
        blocks['blockSizes'].append(it.end-it.begin)

        if trx_rec.strand == '-' :
            qStart = trx_len - (it.end - trx_start)
        else :
            qStart = it.begin - trx_start
            
        blocks['qStarts'].append(qStart)
        
    names = ['matches','misMatches','repMatches','nCount',
             'qNumInsert','qBaseInsert',
             'tNumInsert','tBaseInsert',
             'strand',
             'qName','qSize','qStart','qEnd',
             'tName','tSize','tStart','tEnd',
             'blockCount','blockSizes',
             'qStarts','tStarts']
    vals = [sum(blocks['blockSizes']),-1,-1,-1,
             -1, -1,
            -1, -1,
            trx_rec.strand,
            qName, trx_len, min(blocks['qStarts']), blocks['qStarts'][-1]+blocks['blockSizes'][-1],
            trx_rec.chrm, trx_len, trx_start, trx_end,
            len(blocks['blockSizes']),''.join('{},'.format(_) for _ in blocks['blockSizes']),
            ''.join('{},'.format(_) for _ in blocks['qStarts']),''.join('{},'.format(_) for _ in blocks['tStarts'])
           ]
    return dict(zip(names,vals))
  
if __name__ == '__main__' :
    

    bam_fns = snakemake.input.bams
    psl_fn = snakemake.input.psl
    stats_fns = snakemake.input.stats
        
    bam_stems = [Path(_).stem for _ in bam_fns]
    
    sample_map = {
        'G1715595-1': 'SDE2-1',    
        'G1715595-2': 'SDE2-2',
        'G1715595-3': 'SDE2-input-1',
        'G1715595-4': 'SDE2-input-2',
        'G1715595-5': 'U2AF-1',
        'G1715595-6': 'U2AF-2',
        'G1715595-7': 'U2AF-input-1',
        'G1715595-8': 'U2AF-input-2'
    }

    bams = {k:pysam.AlignmentFile(fn) for k,fn in zip(bam_stems,bam_fns)}

    stats = {}
    for fn in stats_fns :
        samp = fn.replace('_stats.json','')
        fn = Path(fn)
        with open(fn) as f :
            stats[samp] = json.load(f)
    
    regions = pandas.read_csv(psl_fn,
        names=['matches','misMatches','repMatches','nCount',
               'qNumInsert','qBaseInsert',
               'tNumInsert','tBaseInsert',
               'strand',
               'qName','qSize','qStart','qEnd',
               'tName','tSize','tStart','tEnd',
               'blockCount','blockSizes',
               'qStarts','tStarts'],
        #gff names=['chr','source','feature','start','end','score','strand','phase','attr'],
        sep='\t',
        comment='#',
    )
    
    # query_align contains positions relative to the start of the element alignment
    # and records which query coordinates align to the reference
    query_align = defaultdict(Counter)
    
    # coverage contains sample x region read coverage and alignment info
    coverage = defaultdict(dict)
        
    # this loop iterates through each psl alignment record and
    # records the 5' alignment positions of reads that map between
    # the aligned blocks
    for k,r in regions.iterrows() :

        for i in range(r.qSize) :
            query_align['Any'][i] = 1

        coord = '{}:{}-{}'.format(r.tName,r.tStart,r.tEnd)

        qname = r.qName
        if '-' in qname :
            qname = qname.split('-')[1]
        coord = '{}_{}'.format(coord,qname)

        print(coord)
        
        sizes = [int(_) for _ in r.blockSizes.split(',') if len(_) > 0]
        qstarts = [int(_) for _ in r.qStarts.split(',') if len(_) > 0]
        tstarts = [int(_) for _ in r.tStarts.split(',') if len(_) > 0]

        for l,s in zip(sizes,qstarts) :
            for i in range(s,s+l) :
                query_align[coord][i] += 1

        for samp,bam in bams.items() :
            
            name = sample_map[samp]
            
            # norm factor to convert counts into aligned bases per million reads
            norm = stats[samp]['aligned_bases']/1e6
            
            # dictionary records info about the sample x region coverage
            cov_d = defaultdict(float)
            
            # this iterates through the aligned blocks of each total alignment
            for l,qs,ts in zip(sizes,qstarts,tstarts) :

                # cov is a list as long as the block
                cov = [0]*(l+1)
                
                # for reads that overlap this block
                for read in bam.fetch(r.tName,ts,ts+l) :
                    rp = read.reference_start
                    if read.is_reverse :
                        rp = read.get_aligned_pairs(matches_only=True)[-1][1]
                    
                    # for reads with 5' reference alignment position between the
                    # block coordinates, count one for that position within the block coverage
                    if ts <= rp <= ts+l :
                        cov[rp-ts] += 1

                # if the region alignment is against the negative strand,
                # reverse the order of coverage
                if r.strand == '-' :
                    cov = cov[::-1]
                    
                for i,c in zip(range(qs,qs+l),cov) :
                    cov_d['tot_cnts'] += c
                    cov_d[str(i)] += c
                    #query_align[name][i] += c/norm

            if sum(cov_d.values()) > 0 :
                cov_d.update({
                    'chrm':r.tName,
                    'tStart':r.tStart,
                    'tEnd':r.tEnd,
                    'qStart':qstarts[0],
                    'qEnd':qstarts[-1]+sizes[-1],
                    'sample':name,
                    'norm_factor':norm,
                    
                })
                coverage[name+'_'+coord] = dict(cov_d)
            
        #if len(coverage) > 3 :
        #    break
            
    query_align = pandas.DataFrame(query_align).T
    query_align[sorted(query_align.columns)].to_csv(snakemake.output.query_cover,index_label='region')

    coverage = pandas.DataFrame(coverage).T
    coverage_cols = ['chrm','tStart','tEnd','qStart','qEnd','sample','norm_factor','tot_cnts']

    pos_cols = sorted([_ for _ in coverage.columns if _ not in coverage_cols],key=lambda i: int(i))
    coverage[coverage_cols+pos_cols].to_csv(snakemake.output.coverage,index_label='sample_region')