import sys
import csv

fn = sys.argv[1]

gff_fields = ('chrom','source','feature','start','end','score','strand','phase','attributes')
psl_fields = ('matches','misMatches','repMatches','nCount','qNumInsert','qBaseInsert',
        'tNumInsert','tBaseInsert','strand','qName','qSize','qStart','qEnd',
        'tName','tSize','tStart','tEnd','blockCount','blockSizes','qStarts','tStarts')
with open(fn,'rt') as f :
    f = csv.DictReader(f,fieldnames=psl_fields,delimiter='\t')
    sys.stdout.write('##gff-version 3\n')
    out_f = csv.DictWriter(sys.stdout,fieldnames=gff_fields,delimiter='\t')

    for r in f :

        # !!! filter out alignments if matches < 1000 bp or number of blocks is
        # more than 10
        #if int(r['matches']) < 10000 :#or int(r['blockCount']) > 10 :
        #    continue

        '''
        ID=ENST00000456328.2
        geneID=ENSG00000223972.5
        gene_name=DDX11L1
        gene_type=transcript
        transcript_type=processed_transcript
        level=2
        transcript_support_level=1
        tag=basic
        havana_gene=OTTHUMG00000000961.2
        havana_transcript=OTTHUMT00000362751.1
        Name=DDX11L1-202
        '''

        # 'transcript' feature
        tid = '{qName}_{tName}_{tStart}_{tEnd}_trx'.format(**r)
        attrs = {
            'ID': tid,
            'geneID': tid,
            'gene_name': r['qName'],
            'Name': r['qName'],
            'gene_type': 'rDNA_primary_transcript',
            'transcript_type': 'rRNA_primary_transcript',
            'tag': 'basic',
        }

        out_f.writerow({
            'chrom': r['tName'],
            'source': 'NCBI',
            'feature': 'generic_parent',
            'start': r['tStart'],
            'end': r['tEnd'],
            'score': '.',
            'strand': r['strand'],
            'phase': '.',
            'attributes': ';'.join('{}={}'.format(*_) for _ in attrs.items())
        })

        # 'exon' features
        tStarts = r['tStarts'].split(',')
        tStarts = [int(_) for _ in tStarts if len(_) > 0]

        blockSizes = r['blockSizes'].split(',')
        blockSizes = [int(_) for _ in blockSizes if len(_) > 0]

        for i,(start,size) in enumerate(zip(tStarts,blockSizes)) :
            eid = tid+'_{}'.format(i)
            out_f.writerow({
                'chrom': r['tName'],
                'source': 'NCBI',
                'feature': 'match_part',
                'start': start,
                'end': start+size,
                'score': '.',
                'strand': r['strand'],
                'phase': '.',
                'attributes': 'Parent={}'.format(tid)
            })

